package Seminar3;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class App {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        FileSystem fs = FileSystem.get(conf);

        Job job = new Job(conf, "summ on ctn");
        job.setJarByClass(App.class);
        job.setMapperClass(SeminarMapper.class);
        job.setReducerClass(SeminarReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        fs.delete(new Path("data/out"), true);
        FileInputFormat.addInputPath(job, new Path("data/transactions.csv"));
        FileInputFormat.addInputPath(job, new Path("data/users.csv"));
        FileOutputFormat.setOutputPath(job, new Path("data/out"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
