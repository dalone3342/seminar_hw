package Seminar3;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class SeminarReducer extends Reducer<Text, Text, Text, Text> {
    private FloatWritable result = new FloatWritable();
    private Text out = new Text();
    private String nation = "";

    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        float sum = 0;
        for (Text val : values) {
            if (val.toString().charAt(0) == ':')
            {
                nation = val.toString().substring(1);
            } else
                sum += Float.parseFloat(val.toString());
        }
        result.set(sum);
        out.set(result.toString());
        key.set(key.toString() + ":" + nation);
        context.write(key, out);
    }
}