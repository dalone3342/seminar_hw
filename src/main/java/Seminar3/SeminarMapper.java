package Seminar3;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class SeminarMapper extends Mapper<Object, Text, Text, Text> {
    private Text word = new Text();
    private Text wordt = new Text();
    private String va = new String();
    private String[] Va;

    private FloatWritable flt = new FloatWritable();

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        StringTokenizer itr = new StringTokenizer(value.toString());
        va = itr.nextToken();
        if (!va.equals("ctn:date:sum") && !va.equals("ctn:nationality")) {
            Va = va.split(":");
            if (Va.length == 3) {
                word.set(Va[0] + ":" + Va[1].substring(6, 7));
                wordt.set(Va[2].replace(',', '.'));
                context.write(word, wordt);
            }
            else {
                wordt.set(":" + Va[1]);
                word.set(Va[0] + ":1");
                context.write(word, wordt);
                word.set(Va[0] + ":2");
                context.write(word, wordt);
            }
        }

    }
}